<html>
    <head>
        <title>A Site</title>
        <style type="text/css">
            body {
                background-color: black;
                color: white;
            }
            
            table {
                width: 100%;
            }
            
            td {
                vertical-align: top;
            }
            
            .center {
                text-align: center;
            }
        </style>
    </head>
    <body>
        <table>
            <colgroup>
                <col style="width: 15%;" />
                <col style="width: 70%;" />
                <col style="width: 15%;" />
            </colgroup>
            <tr>
                <td id="banner" colspan="3"><?php echo printBanner(); ?></td>
            </tr>
            <tr>
                <td><?php echo printLeft(); ?></td>
                <td><?php echo printContent(); ?></td>
                <td><?php echo printRight(); ?></td>
            </tr>
        </table>
    </body>
</html>

<?php
function printBanner() {
    $html = "<h1 class=\"center\">Header Syntax</h1>";
    return $html;
}

function printleft() {
    $links['SiteLink'] = 'Site.onion';
    $links['SiteLink'] = 'Site.onion';
    
    $html = "<ul>";
    foreach ($links as $key => $value) {
        $html .= "<li><a href=\"$value\">$key</a></li>";
    }
    $html .= "</ul>";
    return $html;
}

function printContent() {
    $html = "<p class=\"center\">centered</p>";
    $html .= "<p>non-centered paragraph</p>";
    $html .= "<p class=\"center\"><img src=\"File.png\" alt=\"\" /></p>";
    return $html;
}

